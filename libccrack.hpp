#include <iostream>                                                                                                                                                                  
#include <cstdlib>
#include "RtMidi.h"
#include <stdio.h>
#include <iostream>
#include <string>

#ifdef GTK
#include <gtk/gtk.h>
#endif

#define NUM_LOOPS 32
#define NUM_STEPS 384
#define REC_CC 107
#define DEL_CC 108
#define DEL_ALL 106



typedef struct loopList
{
  int cc;
  bool active;
  int value[NUM_STEPS];
}loopList;

struct sequencer
{
  RtMidiIn *clockIn;
  RtMidiIn *ccIn;
  RtMidiOut *ccOut;
  struct loopList loops[NUM_LOOPS];
  int currentLoop;
  int step;
  int stepMax;
  int stepCounter;
  bool recording;
  bool playing;
  int lastCC;
  int lastCCNr;
  bool displayChanged;

#ifdef GTK
  GtkWidget *cc_mainWindow;
#endif
};

void cc_updateDisplay();
int cc_loopExists(std::vector< unsigned char > *message);
int cc_newLoop(std::vector< unsigned char > *message);
void cc_sendCC(int cc, int value);
void cc_clockInCallback( double deltatime, std::vector< unsigned char > *message, void *userData );
void cc_ccInCallback( double deltatime, std::vector< unsigned char > *message, void *userData );
int cc_cleanup(sequencer *seq);
int cc_init();

//gtk stuf

