#include <iostream>
#include <cstdlib>
#include "RtMidi.h"
#include <stdio.h>
#include <iostream>
#include <string>
#include "libccrack.hpp"


sequencer mySequencer;
sequencer *seq = &mySequencer;



void cc_updateDisplay()
{
  system("clear");
  printf("CCRACK! \n\n");

 printf("currently selected CC:%u\n\n",seq->loops[seq->currentLoop].cc); 
  printf("Current loops:\n");
  for(int i = 0; i < NUM_LOOPS; i++){
	if(seq->loops[i].active == true){
	  if(seq->recording == true){
		printf("recording cc:%u\n", seq->loops[i].cc);
	  }
      else{
	    printf("playing cc:%u\n", seq->loops[i].cc);
      }
   }
  }
 seq->displayChanged = false;
}

int cc_loopExists(std::vector< unsigned char > *message)
{
   //check if cc exist 
  for(int i = 0; i < NUM_LOOPS; i++){
    if(seq->loops[i].cc == (int)message->at(1)){
      seq->currentLoop = i;
	  return 0; // loop exists
    }
  }
  return 1; // loop doesnt exist
} 

int cc_newLoop(std::vector< unsigned char > *message)
{
   for(int i = 0; i < NUM_LOOPS; i++){
    if(seq->loops[i].cc == 0){
      seq->currentLoop = i;
      seq->loops[seq->currentLoop].cc = (int)message->at(1);
      printf("newloop cc:%u\n", seq->loops[seq->currentLoop].cc);
      return 0;
    }
  }
  return 1;
}

void cc_sendCC(int cc, int value)
{
  std::vector<unsigned char> outMessage;
  //printf("sending ccval %u to ccnr %u\n",value, cc); 
  outMessage.push_back( 176 );
  outMessage.push_back( cc );
  outMessage.push_back( value );
  if(seq->recording == true){
    if(cc != seq->loops[seq->currentLoop].cc)  
      seq->ccOut->sendMessage( &outMessage );
  }
  else{ 
	seq->ccOut->sendMessage( &outMessage );
  }

}

void cc_clockInCallback( double deltatime, std::vector< unsigned char > *message, void *userData )
{

  //printf("message: %u\n", (int)message->at(0));
  if((int)message->at(0) == 250){ // start with jack transport
    seq->playing = true;
	printf("playing\n");
  }
  if((int)message->at(0) == 251){ // start withouth jack transport
    seq->playing = true;
	printf("playing\n");
  }
  if((int)message->at(0) == 252){ // stop
    seq->playing = false;
    seq->step = 1;
    printf("stopped\n");
  }
  if(seq->playing==true){

    std::vector<unsigned char> outMessage;
    if((int)message->at(0) == 248){
       //printf("step: %i bam\n", seq->step);
       if(seq->recording == true){
  	     seq->loops[seq->currentLoop].value[seq->step] = seq->lastCC;
         seq->loops[seq->currentLoop].active = true;

         //printf("writen cc:%u to loopnr: %u on step %u\n", seq->loops[seq->currentLoop].value[seq->step], seq->currentLoop, seq->step);
       }
      for(int i = 0 ; i < NUM_LOOPS ; i++){
        if(seq->loops[i].active == true)
		  cc_sendCC(seq->loops[i].cc, seq->loops[i].value[seq->step]);
      }
      seq->step++;  
      if(seq->step > seq->stepMax)
        seq->step = 1;
     } 
  } // playing
  // update display
  if(seq->step % 48 && seq->displayChanged == true)
	cc_updateDisplay();

}

void cc_ccInCallback( double deltatime, std::vector< unsigned char > *message, void *userData )
{
  if((int)message->at(0) == 250 || (int)message->at(0) == 252 || (int)message->at(0) == 202)
    return;
  if((int)message->at(1) == REC_CC){
    if ((int)message->at(2) == 0){
      seq->recording = false;
	  //printf("recording off\n");
    }
    if ((int)message->at(2) == 127){
	  seq->recording = true;
	  seq->loops[seq->currentLoop].active = true;
	  //printf("recording on\n");
    }
  }
  else if((int)message->at(1) == DEL_CC){
    for(int i = 0; i < seq->stepMax; i++){
	  seq->loops[seq->currentLoop].value[i] = 0;
    }
	seq->loops[seq->currentLoop].active = false;
	printf("cleared cc: %u\n", seq->loops[seq->currentLoop].cc);
  }
  else if((int)message->at(1) == DEL_ALL){
    for(int a = 0; a < NUM_LOOPS; a++){
      for(int i = 0; i < seq->stepMax; i++){
	    seq->loops[a].value[i] = 0;
      }
 	  seq->loops[a].active = false;
    }
  printf("cleared all\n");
  }


  else if(cc_loopExists(message) != 0)
    cc_newLoop(message); 

  if((int)message->at(1) != seq->lastCCNr){
    printf("selected cc:%u\n", seq->loops[seq->currentLoop].cc);
	seq->displayChanged = true;
  }

  if((int)message->at(1) != REC_CC && (int)message->at(1) != DEL_CC)
    seq->lastCC = (int)message->at(2);
    seq->lastCCNr = (int)message->at(1);
  

//  printf("currentLoop: %u\n", seq->currentLoop); 
//  printf("current cc: %u\n", seq->loops[seq->currentLoop].cc);
}

int cc_cleanup(sequencer *seq)
{
  delete seq->clockIn;
  delete seq->ccIn;
  return 0;
}

#ifdef GTK
void cc_buildGui()
{
  gtk_init (NULL, NULL);

  seq->cc_mainWindow = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (seq->cc_mainWindow, "destroy", G_CALLBACK (gtk_main_quit), NULL);
  gtk_widget_show (seq->cc_mainWindow);

}
#endif

int cc_init()
{
  //allocating mem
//  struct loopList *loops;
//  loops = (struct loopList *) malloc( sizeof(struct loopList)*8 );
//  seq->loops = loops;
  seq->playing = false;
  seq->recording = false;
  for(int i = 0; i < NUM_LOOPS; i++){                                                                                                                                          
    seq->loops[seq->currentLoop].cc = 0;
  }

  // setup ports
  seq->clockIn = new RtMidiIn();
  seq->ccIn = new RtMidiIn();
  seq->ccOut = new RtMidiOut();
  // Check available ports.
/*
  unsigned int nPorts = midiin->getPortCount();
  if ( nPorts == 0 ) {
    std::cout << "No ports available!\n";
    //cleanup();
  }
*/
  seq->clockIn->openVirtualPort( "Clock" );
  seq->clockIn->setCallback( &cc_clockInCallback,seq );
  // Don't ignore sysex, timing, or active sensing messages.
  seq->clockIn->ignoreTypes( false, false, false );

  seq->ccIn->openVirtualPort( "CC" );
  seq->ccIn->setCallback( &cc_ccInCallback,seq );
  // Don't ignore sysex, timing, or active sensing messages.
  seq->ccIn->ignoreTypes( true, true, true );
  seq->ccOut->openVirtualPort( "CC" );

  //set some values
  seq->step = 1;
  seq->stepMax = NUM_STEPS;

#ifdef GTK
  cc_buildGui();
  return 0;
#endif

  return 0;
}

