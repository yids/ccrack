CPP=g++
CFLAGS=-g  -Wall -D__LINUX_ALSA__ -lasound -lpthread
GTKFLAGS=`pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

all: ccrack ccrack-gtk ccrack-legacy

ccrack: ccrack.cpp
	$(CPP) -o ccrack ccrack.cpp libccrack.cpp  /home/yids/src/music/python-rtmidi-0.5b1/src/RtMidi.cpp $(CFLAGS)

ccrack-gtk:  ccrack-gtk.cpp
	$(CPP) -DGTK -o ccrack-gtk ccrack-gtk.cpp libccrack.cpp $(GTKFLAGS) /home/yids/src/music/python-rtmidi-0.5b1/src/RtMidi.cpp $(CFLAGS) $(GTKFLAGS)

ccrack-legacy: ccrack-legacy.cpp
	$(CPP) -o ccrack-legacy ccrack-legacy.cpp  /home/yids/src/music/python-rtmidi-0.5b1/src/RtMidi.cpp $(CFLAGS)

clean: 
	rm ccrack ccrack-gtk ccrack-legacy
